import * as firebase from "firebase";
import configiration from "./../config/index";

export class FirebaseServie {
    static mainRef = firebase.database().ref();
    static storage = firebase.storage().ref();

    static saveImageToFirebase(location: string, filename: string, file: Object) {
        console.log(FirebaseServie.storage);
        return new Promise((resolve, reject) => {
            var uploadRef = FirebaseServie.storage.child(location).child(filename).put(file);
            uploadRef.on('state_changed', null, (err) => {
                reject(err)
            }, () => {
                resolve(uploadRef.snapshot.downloadURL)
            });
        });
    }

    static registerUserWithEmail(obj: {email: string, password: string}) {
        return firebase.auth().createUserWithEmailAndPassword(obj.email, obj.password);
    }

    static signinUser(obj: {email: string, password: string}) {
        return firebase.auth().signInWithEmailAndPassword(obj.email, obj.password);
    }

    static signOut() {
        return firebase.auth().signOut();
    }
}