import * as React from "react";
import { connect } from "react-redux";
import { browserHistory, Link } from 'react-router'; // http://stackoverflow.com/questions/31079081/programmatically-navigate-using-react-router
import { GridList, GridTile } from 'material-ui';
import TextField from 'material-ui/TextField';

import GeoLocationAction from '../../store/action/geaLocation';
import AuthAction from '../../store/action/auth';
import { Slider, Searchbar, MapComponent, LoginComponent } from '../../component';

interface IProps extends React.Props<any> {
    location: { lat: number, lng: number };
    getCurrentLocation: () => void;
};

function mapStateToProps(state: any) {
    return {
        location: state.geoLocationReducer['location'],
    };
}

function mapDispatchToProps(dispatch: any) {
    return {
        getCurrentLocation: (): void => dispatch(GeoLocationAction.getCurretLocation()),
    };
}


export class Home extends React.Component<IProps, any> {

    constructor() {
        super();
    }

    componentWillMount() {
        this.props.getCurrentLocation();
    }

    componentDidMount() { }

    componentWillReceiveProps(nextProps) {
        this.setState({ location: nextProps.location });
    }


    onFormSubmit(e: any) {
        // e.preventDefault();
        // browserHistory.push(`/home?q=${document.getElementById("q").value}`);
    }

    componentWillUnmount() { }


    render() {

        return (
            <div>
                <GridList
                    cols={1.3}
                    padding={1}
                    cellHeight={'auto'}
                    style={{ justifyContent: 'center', alignItems: 'center', margin: 0 }} >

                    <Slider />
                    <Searchbar />

                </GridList>
                <div style={{ display: 'flex', width: '100%', marginTop: 3, height: 400 }}>
                    <div style={{ background: '#e4e4e1', flex: 3 }}>
                        <div style={{ margin: 5 }} >
                            <TextField hintText="Events..." style={{ width: '100%' }} />
                        </div>
                        <div style={{ overflow: 'scroll', overflowX: 'hidden', height: "342px" }}>
                            <div style={{ background: '#F2F2ED', margin: 5, height: '100px', display: 'flex', alignItems: 'center' }}>
                                <div style={{ width: '75%', marginRight: 2, marginLeft: 3 }}>
                                    <h5 style={{ margin: 0 }}><strong>The Event Name</strong></h5>
                                    <p style={{ fontSize: '12px' }}>
                                        Short Detail About my Game Event, and where it is. Short Detail About my Game Event, and where it is.
                                </p>
                                </div>
                                <div style={{ width: '25%', marginRight: '5px', display: 'flex', justifyContent: 'flex-end' }}>
                                    <div style={{ width: '80px', height: '92px', backgroundSize: '80px 92px', backgroundImage: 'url(//maps.gstatic.com/tactile/pane/result-no-thumbnail-1x.png)' }}>
                                    </div>
                                </div>
                            </div>

                            <div style={{ background: '#F2F2ED', margin: 5, height: '100px', display: 'flex', alignItems: 'center' }}>
                                <div style={{ width: '75%', marginRight: 2, marginLeft: 3 }}>
                                    <h5 style={{ margin: 0 }}><strong>The Event Name</strong></h5>
                                    <p style={{ fontSize: '12px' }}>
                                        Short Detail About my Game Event, and where it is. Short Detail About my Game Event, and where it is.
                                </p>
                                </div>
                                <div style={{ width: '25%', marginRight: '5px', display: 'flex', justifyContent: 'flex-end' }}>
                                    <div style={{ width: '80px', height: '92px', backgroundSize: '80px 92px', backgroundImage: 'url(//maps.gstatic.com/tactile/pane/result-no-thumbnail-1x.png)' }}>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div style={{ flex: 8 }}>
                        <MapComponent location={this.props.location} />
                    </div>
                </div>
            </div>
        );
    } // render
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);