import * as React from "react";
import { browserHistory } from "react-router";
import { connect } from "react-redux";
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import { GridList, GridTile, Card, TextField, FlatButton } from 'material-ui';

import AuthAction from '../../store/action/auth';

interface IProps extends React.Props<any> {
    showLoader: boolean;
    signupSuccess: boolean;
    registerUser: (obj: any) => void
};

function mapStateToProps(state: any) {
    return {
        showLoader: state.authReducer['showLoader'],
        signupSuccess: state.authReducer['signupSuccess'],
    };
}

function mapDispatchToProps(dispatch: any) {
    return {
        registerUser: (data): void => dispatch(AuthAction.signupUser(data)),
    };
}


// note: React.Component<Properties/Props, component-state>
class Signup extends React.Component<IProps, any> {


    constructor(props) {
        super(props);
        this.state = {
            user: {},
            errorText:""
        };
        this.handleChange = this.handleChange.bind(this);
        this.changeHandler = this.changeHandler.bind(this)
    }

    componentWillMount() {
        // custom rule will have name 'isPasswordMatch'
        ValidatorForm.addValidationRule('isPasswordMatch', (value) => {
            if (value !== this.state.user.password) {
                return false;
            }
            return true;
        });
    }

    handleChange(event) {
        const { user } = this.state;
        user[event.target.name] = event.target.value;

        this.setState({ user });
    }

    handleSubmit() {
        // your submit logic
    }

    changeHandler = (event) => {
        const email = event.target.value;
        this.setState({ email });
    }

    onSignup() {
        console.log('signup firebase');
        // this.props.registerUser(this.state);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.signupSuccess) {
            this.setState({
                username: '',
                email: '',
                password: ''
            });
            alert('Signup Successfuly');
            browserHistory.replace('/home');
        }
    }

    render() {
        const { user, email } = this.state;
        return (
            <ValidatorForm onSubmit={this.onSignup}>
                <GridList
                    cols={1.3}
                    padding={1}
                    cellHeight={'auto'}
                    style={{ justifyContent: 'center', alignItems: 'center', marginTop: 10 }} >

                    <Card style={{ padding: 20, display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                        <h3 style={{ margin: 0, padding: '24px 0 20px', color: 'rgba(0, 0, 0, 0.870588)', fontSize: 22, fontWeight: 400, borderBottom: 'none' }}>
                            Signup Page
                    </h3>
                        <div style={{ width: 400 }}>
                            <TextField
                                name="username"
                                // value={this.state.username}
                                
                                required={true}
                                fullWidth
                                hintText="Yousuf Qutubuddin"
                                floatingLabelText="User Name" />
                            <TextValidator
                                name="email"
                                required={true}
                                value={email}
                                validators={['required', 'isEmail']}
                                errorMessages={['this field is required', 'email is not valid']}
                                onChange={this.changeHandler}
                                fullWidth
                                hintText="usuf52@gmail.com"
                                floatingLabelText="Email" />
                            <TextValidator
                                floatingLabelText="Password"
                                onChange={this.handleChange}
                                fullWidth
                                name="password"
                                type="password"
                                min = {6}
                                validators={['required']}
                                errorMessages={['this field is required']}
                                value={user.password}
                            />
                            <TextValidator
                                floatingLabelText="Repeat password"
                                onChange={this.handleChange}
                                fullWidth
                                name="repeatPassword"
                                type="password"
                                validators={['isPasswordMatch', 'required']}
                                errorMessages={['password mismatch', 'this field is required']}
                                value={user.repeatPassword}
                            />
                            <div style={{ marginTop: 30, display: 'flex', justifyContent: 'flex-end' }}>
                                <FlatButton hoverColor="red" backgroundColor="#D0191D" label="Signup" type="submit" style={{ color: 'white' }} />
                                {this.props.showLoader ? 'Loading....' : ''}
                            </div>
                        </div>
                    </Card>

                </GridList>
            </ValidatorForm>
        )

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Signup);