import * as React from "react";
import { connect } from "react-redux";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import AuthAction from '../../store/action/auth';
import { Navbar } from "./../../component/index";
import { LoginComponent } from '../../component';

interface IProps extends React.Props<any> {
    loginModal: boolean;
    isAuthenticated: boolean;
    showLoader: boolean;
    errMsg: string | Object;
    openLoginModal: () => void;
    closeLoginModal: () => void;
    login: (o: any) => void;
    logout: () => void;
};

function mapStateToProps(state: any) {
    return {
        loginModal: state.authReducer['loginModal'],
        isAuthenticated: state.authReducer['isAuthenticated'],
        showLoader: state.authReducer['showLoader'],
        errMsg: state.authReducer['errMsg'],
    };
}

function mapDispatchToProps(dispatch: any) {
    return {
        openLoginModal: (): void => dispatch(AuthAction.openLoginModal()),
        closeLoginModal: (): void => dispatch(AuthAction.closeLoginModal()),
        login: (o): void => dispatch(AuthAction.signinUser(o)),
        logout: (): void => dispatch(AuthAction.signinOut()),
    };
}


// note: React.Component<Properties/Props, component-state>
class App extends React.Component<IProps, any> {

    constructor() {
        super();
    }

    closeLoginModal() {
        this.props.closeLoginModal();
    }

    render() {
        return (
            <MuiThemeProvider>
                <div>
                    <Navbar loginModal={this.props.openLoginModal} isAuthenticated={this.props.isAuthenticated} logout={this.props.logout} />
                    {/* add this for show routes*/}
                    {this.props.children}

                    <div id="applicatioModals">
                        <LoginComponent open={this.props.loginModal} close={this.closeLoginModal.bind(this)} onSubmit={this.props.login} />
                    </div>

                </div>
            </MuiThemeProvider>
        )

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(App);