import AuthAction from '../action/auth';

// import { fromJS } from 'immutable';
// const INITIAL_STATE = fromJS({
//   count: 0,
// });

const INITIAL_STATE = {
    showLoader: false,
    isAuthenticated: false,
    loginModal: false,
    errMsg: null,
    signupSuccess: false
};

interface IAction {
    type: string,
    payload?: any
}

export default function authReducer(state = INITIAL_STATE, action: IAction) {
    switch (action.type) {

        case AuthAction.OPENLOGINMODAL:
            return Object.assign({}, state, { loginModal: true });

        case AuthAction.CLOSELOGINMODAL:
            return Object.assign({}, state, { loginModal: false });

        case AuthAction.SIGNUPUSER:
        case AuthAction.SIGNINUSER:
        case AuthAction.SIGNOUT:
            return Object.assign({}, state, { showLoader: true });

        case AuthAction.SIGNUPUSER_SUCCESS:
            return Object.assign({}, state, { showLoader: false, errMsg: null, signupSuccess: true });
        case AuthAction.SIGNUPUSER_FAIL:
            return Object.assign({}, state, { showLoader: false, errMsg: action.payload.errMsg });

        case AuthAction.SIGNINUSER_SUCCESS:
            return Object.assign({}, state, { showLoader: false, errMsg: null, isAuthenticated: true });
        case AuthAction.SIGNINUSER_FAIL:
            return Object.assign({}, state, { showLoader: false, errMsg: action.payload.errMsg, isAuthenticated: false });

        case AuthAction.SIGNOUT_SUCCESS:
            return Object.assign({}, state, { showLoader: false, errMsg: null, isAuthenticated: false });
        case AuthAction.SIGNOUT_FAIL:
            return Object.assign({}, state, { showLoader: false, errMsg: action.payload.errMsg });        

        default:
            return state;
    }
}