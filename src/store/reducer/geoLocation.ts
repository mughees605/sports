import GeoLocationAction from '../action/geaLocation';

// import { fromJS } from 'immutable';
// const INITIAL_STATE = fromJS({
//   count: 0,
// });

const INITIAL_STATE = {
    location: { lat: 59.955413, lng: 30.337844 }
};

interface IAction {
    type: string,
    payload?: any
}

export default function geoLocationReducer(state = INITIAL_STATE, action: IAction) {
    switch (action.type) {

        case GeoLocationAction.CURRENTLOCATION_SUCCESS:
            return Object.assign({}, state, { location: action.payload });

        case GeoLocationAction.CURRENTLOCATION_FAILURE:
            return Object.assign({}, state, { location: { lat: 24.9991316, lng: 66.5042376 } });

        default:
            return state;
    }
}