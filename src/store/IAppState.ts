// Application State Interface
interface IAppState {
    geoLocationReducer: Object;
    authReducer: Object;
}

export default IAppState;