import { store } from '../index';

export default class AuthAction {

    static OPENLOGINMODAL = 'OPENLOGINMODAL';
    static CLOSELOGINMODAL = 'CLOSELOGINMODAL';

    static SIGNUPUSER = 'SIGNUPUSER';
    static SIGNUPUSER_SUCCESS = 'SIGNUPUSER_SUCCESS';
    static SIGNUPUSER_FAIL = 'SIGNUPUSER_FAIL';

    static SIGNINUSER = 'SIGNINUSER';
    static SIGNINUSER_SUCCESS = 'SIGNINUSER_SUCCESS';
    static SIGNINUSER_FAIL = 'SIGNINUSER_FAIL';

    static SIGNOUT = 'SIGNOUT';
    static SIGNOUT_SUCCESS = 'SIGNOUT_SUCCESS';
    static SIGNOUT_FAIL = 'SIGNOUT_FAIL';

    static NULL = 'NULL'

    static openLoginModal() {
        return {
            type: AuthAction.OPENLOGINMODAL
        };
    }

    static closeLoginModal() {
        return {
            type: AuthAction.CLOSELOGINMODAL
        };
    }

    static signupUser(payload) {
        return {
            type: AuthAction.SIGNUPUSER,
            payload
        };
    }

    static signinUser(payload) {
        return {
            type: AuthAction.SIGNINUSER,
            payload
        };
    }

    static signinOut() {
        return {
            type: AuthAction.SIGNOUT,
        };
    }

}
