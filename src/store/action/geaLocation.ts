import { store } from '../index';

export default class GeoLocationAction {

    static CURRENTLOCATION = 'CURRENTLOCATION';
    static CURRENTLOCATION_SUCCESS = 'CURRENTLOCATION_SUCCESS';
    static CURRENTLOCATION_FAILURE = 'CURRENTLOCATION_FAILURE';
    static NULL = 'NULL'

    static getCurretLocation() {
        return {
            type: GeoLocationAction.CURRENTLOCATION
        };
    }

}
