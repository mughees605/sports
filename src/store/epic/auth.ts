import { Observable } from 'rxjs';
import { ActionsObservable } from 'redux-observable';
import AuthAction from '../action/auth';
import { FirebaseServie } from '../../service/firebase';

class AuthEpic {

    signupUser = (action$: ActionsObservable<any>) =>
        action$.ofType(AuthAction.SIGNUPUSER)
            .switchMap(({payload}) => {
                return Observable.fromPromise(FirebaseServie.registerUserWithEmail(payload))
                    .catch(err => {
                        return this.errorObservable(AuthAction.SIGNUPUSER_FAIL, err.message)
                    })
                    .map((obj: any) => {
                        if (obj.type === 'SIGNUPUSER_FAIL') {
                            return obj;
                        } else {
                            return {
                                type: AuthAction.SIGNUPUSER_SUCCESS,
                                payload: obj
                            }
                        }
                    })
            });

    signinUser = (action$: ActionsObservable<any>) =>
        action$.ofType(AuthAction.SIGNINUSER)
            .switchMap(({payload}) => {
                return Observable.fromPromise(FirebaseServie.signinUser(payload))
                    .catch(err => {
                        return this.errorObservable(AuthAction.SIGNINUSER_FAIL, err.message)
                    })
                    .map((obj: any) => {
                        if (obj.type === 'SIGNUPUSER_FAIL') {
                            return obj;
                        } else {
                            return {
                                type: AuthAction.SIGNINUSER_SUCCESS,
                                payload: obj
                            }
                        }
                    })
            });

    signOut = (action$: ActionsObservable<any>) =>
        action$.ofType(AuthAction.SIGNOUT)
            .switchMap(({payload}) => {
                return Observable.fromPromise(FirebaseServie.signOut())
                    .catch(err => {
                        return this.errorObservable(AuthAction.SIGNOUT_FAIL, err.message)
                    })
                    .map((obj: any) => {
                        if (obj && obj.type === 'SIGNOUT_FAIL') {
                            return obj;
                        } else {
                            return {
                                type: AuthAction.SIGNOUT_SUCCESS
                            }
                        }
                    })
            });

    errorObservable(type, errMsg): Observable<{ type: string, payload: Object }> {
        return Observable.of({
            type,
            payload: { errMsg }
        });
    }

}

let authEpic = new AuthEpic();
export default authEpic;