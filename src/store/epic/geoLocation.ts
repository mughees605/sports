import { Observable } from 'rxjs';
import { ActionsObservable } from 'redux-observable';
import GeoLocationAction from '../action/geaLocation';

class GeoLocationEpic {

    getCurrentLocation = (action$: ActionsObservable<any>) =>
        action$.ofType(GeoLocationAction.CURRENTLOCATION)
            .switchMap(() => {
                return Observable.fromPromise(new Promise(res => {
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition((position) => {
                            // console.info('navigator.geolocation has found! ', position);
                            // currentLocation.lat = position.coords.latitude;
                            // currentLocation.lng = position.coords.longitude;
                            // infoWindow.setPosition(pos);
                            // infoWindow.setContent('Location found.');
                            // map.setCenter(pos);
                            res({ lat: position.coords.latitude, lng: position.coords.longitude })
                        }, (err) => {
                            console.error('Error: to get Current Position: ', err)
                            // this.handleLocationError(true, infoWindow, map.getCenter());
                            res(null)
                        });
                    } else {
                        console.error('navigator.geolocation has not found!')
                        // this.currentLocation = { lat: 0.00, lng: 0.00 };
                        // Browser doesn't support Geolocation
                        res(null);
                        // return Observable.of({
                        //     type: GeoLocationAction.CURRENTLOCATION_FAILURE
                        // });
                    }
                })).map((d) => {
                    if (d) {
                        return {
                            type: GeoLocationAction.CURRENTLOCATION_SUCCESS,
                            payload: d
                        }
                    } else {
                        return {
                            type: GeoLocationAction.CURRENTLOCATION_FAILURE
                        }
                    }


                })
            });

}

let geoLocationEpic = new GeoLocationEpic();
export default geoLocationEpic;