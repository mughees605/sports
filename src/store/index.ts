// https://redux-observable.js.org/docs/basics/SettingUpTheMiddleware.html
import { combineReducers, createStore, applyMiddleware } from 'redux';
import { combineEpics, createEpicMiddleware } from 'redux-observable';

// Application State IAppState
import IAppState from './IAppState';

// reducers
import geoLocationReducer from './reducer/geoLocation';
import authReducer from './reducer/auth';

// epics
import geoLocationEpic from './epic/geoLocation';
import authEpic from './epic/auth';

// Application Epics / Effects
const rootEpic = combineEpics(
    geoLocationEpic.getCurrentLocation,
    authEpic.signupUser,
    authEpic.signinUser,
    authEpic.signOut,
);

// Application Reducers
export const rootReducer = combineReducers<IAppState>({
    geoLocationReducer,
    authReducer
});


const epicMiddleware = createEpicMiddleware(rootEpic);

const createStoreWithMiddleware = applyMiddleware(epicMiddleware)(createStore);

export let store = createStoreWithMiddleware(rootReducer);
