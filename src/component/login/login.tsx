import * as React from "react";
import { Link } from "react-router";

import { Dialog, FlatButton, RaisedButton, TextField } from 'material-ui'

interface IProps extends React.Props<any> {
    open: boolean;
    close: () => void;
    onSubmit: (o: {email: string, password: string}) => void;
};

export default class LoginComponent extends React.Component<IProps, any> {

    

    constructor(props) {
        super(props)
        this.state = {
        email: "",
        password: "",
        errorText:"",
        button:false
       
    }
    }

    onChangeHandler = (ev) => {
        var check =  /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if(!check.test(ev.target.value)){
            this.setState({errorText:"Please provide correct email",button:false})
        }
        else {
            this.setState({errorText:"",password:"Please type password"})
        }
    }
    onPassword = (ev)=>{
         if(ev.target.value.length > 0){
             this.setState({
                 button:true
             })
         }
         else{
             this.setState({
                 button:false,

                 password:""
             })
         }
         
    }

    onSubmit = () => {
        this.props.close();
        this.props.onSubmit(this.state);
        this.setState({
            button:false
        })
    }

    componentWillReceiveProps() { }

    render() {

        const actions = [
            <FlatButton
                label="Cancel"
                primary={true}
                onTouchTap={this.props.close} />,
            <FlatButton
                label="Login"
                secondary={true}
                keyboardFocused={true}
                disabled = {!this.state.button}
                onTouchTap={this.onSubmit} />,
        ];

        return (
            <Dialog
                title='Login'
                contentStyle={{ width: 400 }}
                actions={actions}
                modal={true}
                open={this.props.open}
                onRequestClose={this.props.close} >
                <div style={{ display: 'flex', flexDirection: "column" }}>
                    <TextField
                        name="email"
                        onChange={this.onChangeHandler}
                        fullWidth
                        errorText = {this.state.errorText}
                        hintText="usuf52@gmail.com"
                        floatingLabelText="Email" />
                    <TextField
                        name="password"
                        onChange = {this.onPassword}
                        fullWidth
                        errorText = {this.state.password}
                        type="password"
                        hintText="*****"
                        floatingLabelText="Passowrd" />
                    <div style={{ marginTop: 33, display: 'flex', justifyContent: 'flex-end' }}>
                        <Link to="/signup" onClick={this.props.close}>Don't have an account?</Link>
                    </div>
                </div>
            </Dialog>
        );
    }
}

