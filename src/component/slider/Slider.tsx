import * as React from "react";

import { Card } from 'material-ui';

export default class SliderComponent extends React.Component<any, any> {

    images = ['assets/slide_01.jpg','assets/slide_02.jpg','assets/slide_03.jpg','assets/slide_04.png']
    state = {
        image: 'assets/slide_01.jpg'
    }

    timer: any;
    componentWillMount() {
        this.timer = setInterval(() => {
            var number = Math.floor(Math.random() * 3 + 1);
            this.setState({image: this.images[Math.floor(Math.random() * 3 + 1)]})
        }, 4000)
    }

    componentWillReceiveProps() { 

    }

     componentWillUnmount() { 
        clearInterval(this.timer);
     }

    render() {

        return (
            <Card style={{marginTop: '2px'}}>
                <div style={{backgroundColor: 'lightgrey', height: '30vh', display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
                    {/*<span style={{fontSize: '350%', color: 'white'}}>Slider</span>*/}
                    <img src={this.state.image} alt={this.state.image} style={{height: "31vh", width: "100%"}} />
                </div>
            </Card>
        );
    }
}