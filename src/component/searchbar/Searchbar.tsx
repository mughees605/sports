import * as React from "react";

import { TextField, SelectField, MenuItem, FlatButton, Card } from 'material-ui';


export default class SearchBarComponent extends React.Component<any, any> {

    categories = {
        "Select Category": ["Select Game"],
        "Children's": [
            "Candy Land",
            "Catan Junior",
            "Chicken Cha Cha Cha",
            "Don't Break the Ice",
            "Don't Wake Daddy",
            "Forbidden Bridge",
            "Hey Pa! There's a Goat on the Roof",
            "Hi Ho! Cherry-O",
            "Hungry Hungry Hippos",
            "The Kids of Catan",
            "Ludo",
            "Mouse Trap",
            "Snakes and ladders (aka Chutes and Ladders)",
            "Sorry!",
            "Space Hop",
            "Uncle Wiggily",
        ],
        "Cooperative": [
            "Arkham Horror",
            "Battlestar Galactica: The Board Game",
            "Betrayal at House on the Hill",
            "Castle Panic",
            "Defenders of the Realm",
            "Forbidden Island",
            "Freedom: The Underground Railroad",
            "Hanabi",
            "Legendary: A Marvel Deck Building Game",
            "Lord of the Rings",
            "Pandemic",
            "Space Cadets",
            "Sentinels of the Multiverse",
            "Shadows Over Camelot",
            "Space Alert",
            "X-COM"
        ],
        "Economics and strategy": [
            "Acquire",
            "Junta",
            "Kolejka",
            "København",
            "Monopoly",
            "Power Grid",
            "Risk",
            "Risk 2210 AD",
            "Sentinels of the Multiverse",
            "Squatter",
            "The Settlers of Catan",
            "Ticket to Ride"
        ],
        "European race": [
            "Cartagena",
            "Chicken Cha Cha Cha",
            "Formula Dé",
            "Game of the Goose",
            "Hare and Tortoise",
            "Mississippi Queen",
            "Transformers"
        ],
        "Games of physical skill": [
            "Blockhead!",
            "Buckaroo",
            "Camp Granada",
            "Carrom",
            "Chapayev",
            "Crokinole",
            "Dart Wars",
            "Gnip Gnop",
            "Hungry Hungry Hippos",
            "Jenga",
            "Kerplunk",
            "Krazy Maze",
            "Operation",
            "Perfection",
            "Polarity",
            "Subbuteo",
            "Twister",
            "Villa Paletti"
        ],
        "Gaming systems": [
            "Flibbix",
            "Hanafuda",
            "Icehouse pieces",
            "Stonehenge"
        ],
        "Multi-player elimination": [
            "18XX",
            "221B Baker Street",
            "30 Seconds",
            "About Time",
            "Acquire",
            "Acronymble",
            "Adel Verpflichtet",
            "Afrikan tähti",
            "Agricola",
            "Air Charter",
            "Aksharit",
            "Aladdin's Dragons",
            "Alhambra",
            "Alias",
            "Amun-Re",
            "Arkham Horror",
            "Articulate!",
            "Ashta Chamma",
            "Auf Achse",
            "Australia",
            "Bailout! The Game",
            "Balderdash",
            "Barbarossa",
            "Battlestar Galactica Board Game & Expansions", ,
            "Beat the Deal",
            "Bezzerwizzer",
            "Blankety Blank",
            "Bleff",
            "Blokus",
            "(This Game Is) Bonkers!",
            "Brain Chain",
            "Brainstorm",
            "Break the Safe",
            "Buccaneer",
            "Camel Up",
            "Candy Land",
            "Can't Stop",
            "Capitol",
            "Carcassonne",
            "Careers",
            "Caribbean",
            "Cartagena",
            "Caylus",
            "Chromino",
            "CirKis",
            "Civilization",
            "Clans",
            "Clue/Cluedo",
            "Codenames",
            "Colt Express",
            "Concept",
            "Conspiracy",
            "Cosmic Encounter",
            "Cranium",
            "Crosstrack",
            "Dark Tower",
            "Dead of Winter: A Cross Roads Game",
            "Diamant",
            "Dígalo con lápiz",
            "Dixit",
            "Domaine",
            "Dominion",
            "Don't Miss The Boat",
            "Don't Quote Me",
            "Dorn",
            "Drunter und Drüber",
            "Dune",
            "Dungeons & Dragons",
            "El Grande",
            "Elfenland",
            "Enchanted Forest",
            "Entdecker",
            "Escape from Atlantis",
            "es:Estanciero (juego)",
            "Evo",
            "Experimental",
            "Farlander",
            "Fictionary",
            "Figure It Out",
            "Fireball Island",
            "Focus",
            "Fresco",
            "Game For Fame the Party Board Game",
            "GiftTRAP",
            "Giganten",
            "Girl Talk",
            "Go",
            "Great Train Robbery",
            "GridIron Master",
            "History of the World",
            "Hooop!",
            "Husker Du?",
            "I'm the Boss!",
            "Imperial",
            "Indicios",
            "Indigo",
            "Ingenious",
            "Inkan aarre",
            "Java",
            "Journey through Europe", ,
            "Junta",
            "Kill Doctor Lucky",
            "Kingdoms",
            "Landslide",
            "Las Vegas",
            "Le Havre",
            "Life",
            "Logo Board Game",
            "The London Game",
            "Lords of Waterdeep",
            "Lost Cities Board Game",
            "Löwenherz",
            "Luck of the Draw",
            "Die Macher",
            "The Mad Magazine Game",
            "The Magic Labyrinth",
            "Mahjong",
            "Malefiz",
            "Mall Madness",
            "Manhattan",
            "Master Labyrinth",
            "Masterpiece",
            "Medici",
            "Medina",
            "Mensch ärgere dich nicht",
            "Merchant of Venus",
            "Mexica",
            "Mine a million",
            "Modern Art",
            "Mutant Meeples",
            "Niagara",
            "Okey",
            "Omega Virus",
            "Ouija",
            "Outrage!",
            "Pack & Stack",
            "Pandemic",
            "Parcheesi",
            "Parqués",
            "Pay Day",
            "Personal Preference",
            "Petit Microscope",
            "Pictionary",
            "Pirate's Cove",
            "Power Grid",
            "Primordial Soup",
            "Princes of Florence",
            "Puerto Rico",
            "Puzzle",
            "Qin",
            "Quoridor",
            "Qwirkle",
            "Ra",
            "Rail Baron",
            "Rappakalja",
            "Razzia",
            "The Really Nasty Horse Racing Game",
            "Ricochet Robots",
            "Rivers, Roads & Rails",
            "RoboRally",
            "Roulette",
            "Rummikub",
            "Rummoli",
            "Saint Petersburg",
            "Samurai",
            "San Marco",
            "Scattergories",
            "Scene It",
            "Scotland Yard",
            "Scoundrels of Skullport",
            "Scrabble",
            "Sequence",
            "The Settlers of Catan",
            "Shadows over Camelot",
            "Sherlock Holmes: Consulting Detective",
            "Skirrid",
            "Small World",
            "Snakes and Ladders",
            "Sorry!",
            "Splendor",
            "Squatter",
            "Stock Ticker",
            "Taj Mahal",
            "Take It Easy",
            "Take Off!",
            "Terra Mystica",
            "Through the Desert",
            "Thurn and Taxis",
            "Ticket to Ride",
            "Tigris & Euphrates",
            "Tikal",
            "Timberland",
            "Time's Up!",
            "Top Secret Spies",
            "Torres",
            "Totopoly",
            "Tracks to Telluride",
            "TransAmerica",
            "Trivia Crack",
            "Trivial Pursuit",
            "Trouble",
            "Twilight Imperium",
            "Twin Tin Bots",
            "Ubongo",
            "Upwords",
            "Vanished Planet",
            "Vinci",
            "Yahtzee",
            "Yut",
            "Zombies!!!",
            "Zoophoria"
        ],
        "Multiplayer games without el,imination": [
            "13 Dead End Drive",
            "1313 Dead End Drive",
            "American Megafauna",
            "Anti-Monopoly",
            "Attack!",
            "Axis & Allies",
            "Bang!",
            "Battle Sheep",
            "Beat the Deal",
            "Betrayal at House on the Hill",
            "Blokus",
            "Blood Feud in New York",
            "Blue Max",
            "Bookchase",
            "Castle Risk",
            "Clue Jr.: Case of the Missing Pet",
            "Coppit",
            "Crash! The bankrupt game",
            "Diplomacy",
            "Djambi",
            "Doom: The Boardgame",
            "Eclipse",
            "Finance",
            "Ghettopoly",
            "The Great Train Robbery",
            "Heroscape",
            "Hey, That's My Fish!",
            "Hotels",
            "Jenga",
            "Khet",
            "King of Tokyo",
            "King Oil",
            "Ludo",
            "Mikado",
            "Monopoly",
            "Monopoly Junior",
            "Poleconomy",
            "Risk",
            "Shadow Hunters",
            "Shogun/Samurai Swords",
            "Solarquest",
            "Spy Alley",
            "Star Wars Epic Duels",
            "Star Wars Tactics",
            "StarCraft: The Board Game",
            "Strange Synergy",
            "Summit",
            "TEG",
            "Titan",
            "Tsuro",
            "Tsuro of the Seas",
            "War on Terror"
        ],
        "Two-player": [
            "Abalone",
            "Agon",
            "Asalto",
            "Backgammon",
            "Battleship",
            "Blockade",
            "Blokus",
            "Blood Bowl",
            "Bul",
            "Camelot",
            "Checkers",
            "Chess",
            "Clue",
            "Connect 4",
            "Cross and Circle game",
            "Daldøs",
            "Diamond",
            "Downfall",
            "DVONN",
            "Fanorona",
            "Forbidden desert",
            "Game of the Generals",
            "Ghosts",
            "Go",
            "Gipf",
            "Gravity maze",
            "Guess Who?",
            "Hare and Hounds",
            "Hedbanz",
            "Hex",
            "Hidden hints",
            "Hijara",
            "Hungry hippo",
            "Isola",
            "Janggi (Korean chess)",
            "Le Jeu de la Guerre",
            "Kalah",
            "Kamisado",
            "Liu po",
            "Lost Cities",
            "Mad Gab",
            "Master Mind",
            "Matching game",
            "Mix'n blast volcano",
            "Nine Men's Morris",
            "Obsession",
            "Onyx",
            "Operation",
            "Payday jour de paye",
            "Plateau",
            "PÜNCT",
            "Quarto",
            "Qwirkle",
            "Rithmomachy",
            "Sáhkku",
            "Santorini",
            "Scooby-doo! Big roll bingo",
            "Scrabble",
            "Senet",
            "Shogi",
            "Simon",
            "Skip bo",
            "Space Hulk",
            "Spot it!",
            "Spot it jr",
            "Stratego",
            "Sudoko advance",
            "Sugoroku",
            "Tâb",
            "Tablut",
            "Tantrix",
            "The secret door",
            "Turn to lubeck",
            "Twilight Struggle",
            "Ultimatrix battle game",
            "Wari",
            "Wooper",
            "World geo puzzle",
            "Wubble",
            "Xiangqi (Chinese chess)",
            "Yahtzee jr.",
            "YINSH",
            "ZÈRTZ",
            "Zingo!"
        ]
    };

    state = {
        category: 'Select Category',
        game: 'Select Game',
        status: 'Status',
        radius: 'Radius',
        eventDuration: 'Event Duration',
        listedTill: 'Listed Till'
    }

    handleChange(event, index, value, target) {
        this.setState({ [target]: value }, () => {
            if (target == 'category') {
                this.setState({ 'game': this.categories[this.state.category][0] });
            }
        });
    }

    componentWillReceiveProps() { }

    render() {
        const categoryMenu = Object.keys(this.categories).map((v, i) => {
            return <MenuItem key={i} value={v} primaryText={v} />
        });
        const gameMenu = this.categories[this.state.category].map((v, i) => {
            return <MenuItem key={i} value={v} primaryText={v} />
        });

        return (
            <Card>
                <div style={{ paddingBottom: '10px', height: 'auto', display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
                    {/*<span style={{ fontSize: '350%', color: 'white' }}>Search Bar</span>*/}
                    <div style={{ width: '90%', textAlign: 'center' }}>
                        <TextField
                            style={{ width: '100%' }}
                            hintText="Game Name"
                            floatingLabelText="Where are you looking??"
                        />
                    </div>
                    <div style={{ marginTop: '10px', display: 'flex', flexWrap: 'wrap', justifyContent: 'center', width: '90%' }}>
                        <SelectField
                            style={{ marginLeft: '10px' }}
                            // floatingLabelText="Frequency"
                            value={this.state.category}
                            onChange={(e, i, v) => this.handleChange(e, i, v, 'category')}
                            autoWidth={true} >
                            {categoryMenu}
                        </SelectField>
                        <SelectField
                            style={{ marginLeft: '10px' }}
                            // floatingLabelText="Frequency"
                            value={this.state.game}
                            onChange={(e, i, v) => this.handleChange(e, i, v, 'game')}
                            autoWidth={true} >
                            {gameMenu}
                        </SelectField>
                        <SelectField
                            style={{ marginLeft: '10px' }}
                            // floatingLabelText="Frequency"
                            value={this.state.status}
                            onChange={(e, i, v) => this.handleChange(e, i, v, 'status')}
                            autoWidth={true} >
                            <MenuItem value='Status' primaryText="Status" />
                            <MenuItem value='Open' primaryText="Open" />
                            <MenuItem value='Close' primaryText="Close" />
                        </SelectField>
                        <SelectField
                            style={{ marginLeft: '10px' }}
                            // floatingLabelText="Frequency"
                            value={this.state.radius}
                            onChange={(e, i, v) => this.handleChange(e, i, v, 'radius')}
                            autoWidth={true} >
                            <MenuItem value="Radius" primaryText="Radius" />
                            <MenuItem value='1 km' primaryText="1 Km" />
                            <MenuItem value='2 km' primaryText="2 Km" />
                        </SelectField>
                        <SelectField
                            style={{ marginLeft: '10px' }}
                            // floatingLabelText="Frequency"
                            value={this.state.eventDuration}
                            onChange={(e, i, v) => this.handleChange(e, i, v, 'eventDuration')}
                            autoWidth={true} >
                            <MenuItem value='Event Duration' primaryText="Event Duration" />
                            <MenuItem value='30 mins' primaryText="30 mins" />
                            <MenuItem value='1 Hour' primaryText="1 Hour" />
                            <MenuItem value='2 Hour' primaryText="2 Hour" />
                            <MenuItem value='3 Hour' primaryText="3 Hour" />
                            <MenuItem value='4 Hour' primaryText="4 Hour" />
                        </SelectField>
                        <SelectField
                            style={{ marginLeft: '10px' }}
                            // floatingLabelText="Frequency"
                            value={this.state.listedTill}
                            onChange={(e, i, v) => this.handleChange(e, i, v, 'listedTill')}
                            autoWidth={true} >
                            <MenuItem value="Listed Till" primaryText="Listed Till" />
                            <MenuItem value="2 hours" primaryText="2 Hours" />
                            <MenuItem value="4 hours" primaryText="4 Hours" />
                            <MenuItem value="8 hours" primaryText="8 Hours" />
                            <MenuItem value="16 hours" primaryText="16 Hours" />
                            <MenuItem value="1 Day" primaryText="1 Days" />
                            <MenuItem value="2 Days" primaryText="2 Days" />
                            <MenuItem value="3 Days" primaryText="3 Days" />
                            <MenuItem value="1 Week" primaryText="1 Weeks" />
                            <MenuItem value="2 Weeks" primaryText="2 Weeks" />
                        </SelectField>
                    </div>
                    <div style={{ marginTop: '10px', display: 'flex', alignItems: 'flex-end', justifyContent: 'flex-end', width: '90%' }}>
                        <FlatButton style={{ backgroundColor: '#D0191D', color: 'white' }} label="Search" />
                    </div>
                </div>
            </Card>
        );
    }
}