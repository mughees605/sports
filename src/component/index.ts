import Navbar from "./navbar/Navbar";
import Slider from "./slider/Slider";
import Searchbar from "./searchbar/Searchbar";
import MapComponent from "./map/map";
import LoginComponent from "./login/login";

export {
    Navbar,
    Slider,
    Searchbar,
    MapComponent,
    LoginComponent
}