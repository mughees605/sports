import * as React from "react";
import GoogleMapReact from 'google-map-react';

const AnyReactComponent = ({ text, lat, lng }) => <div style={{ color: 'red', background: 'yellow' }}>{text}</div>;

interface IProps extends React.Props<any> {
    location: {lat: number, lng: number };
};

export default class MapComponent extends React.Component<IProps, any> {

    componentWillReceiveProps() { }

    render() {
        return (
            <GoogleMapReact
                // bootstrapURLKeys={'AIzaSyCKBZB29HTvA-GL70CiF25bdGo75VGwuUg'}
                apiKey={'AIzaSyCKBZB29HTvA-GL70CiF25bdGo75VGwuUg'}
                onChange={(e, a) => {
                    console.log('onMapChange: e, a, this: ')
                }}
                // defaultCenter={this.props.location}
                center={this.props.location}
                defaultZoom={12} >
                <AnyReactComponent
                    lat={this.props.location.lat + 10}
                    lng={this.props.location.lng + 10}
                    text={'Kreyser Avrora'} />
            </GoogleMapReact>
        );
    }
}

