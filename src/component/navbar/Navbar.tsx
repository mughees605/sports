import * as React from "react";
import { Link } from "react-router";

import LoginComponent from '../login/login';

import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import FlatButton from 'material-ui/FlatButton';
import Toggle from 'material-ui/Toggle';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import NavigationClose from 'material-ui/svg-icons/navigation/close';


interface IProps {
    loginModal: () => void;
    isAuthenticated: boolean;
    logout: () => void;
}

export default class NavbarComponent extends React.Component<IProps, any> {

    state = {
        logged: true,
    };

    handleChange = (event, logged) => {
        this.setState({ logged: logged });
    };


    componentWillReceiveProps() { }

    render() {
        return (
            <div>
                <AppBar
                    style={{ backgroundColor: '#D0191D' }}
                    iconElementLeft={<Link to="/home" style={{ display: 'inline-block', marginTop: 11 }}><img style={{ marginLeft: 10, marginRight: 10 }} src="assets/icons/home_24px.svg" alt="" /></Link>}
                    title="Sports Meetup"
                    iconElementRight={this.props.isAuthenticated ? <Logged logout={this.props.logout} /> : <Login loginModal={this.props.loginModal} />} />
                {/*<Toggle
                    label="Logged"
                    defaultToggled={true}
                    onToggle={this.handleChange}
                    labelPosition="right"
                    style={{ margin: 20 }}
                />*/}
            </div>
        );
    }
}


class Login extends React.Component<any, any> {
    render() {
        return (
            <FlatButton style={{ color: 'white' }} label="Login" onClick={this.props.loginModal} />
        );
    }
}


const Logged = (props) => (
    <div style={{ display: 'flex', alignItems: 'center' }}>
        <span style={{ color: 'white', display: 'inline-block' }}>
            <Link to="/event"><FlatButton style={{ color: 'white' }} label="Register Event" /></Link>
            {/*<FlatButton style={{ color: 'white' }} label="Login" />*/}
        </span>
        <IconMenu
            // {...props}
            iconButtonElement={
                <IconButton><MoreVertIcon /></IconButton>
            }
            targetOrigin={{ horizontal: 'right', vertical: 'top' }}
            anchorOrigin={{ horizontal: 'right', vertical: 'top' }} >
            <MenuItem primaryText="Refresh" />
            <MenuItem primaryText="Help" />
            <MenuItem primaryText="Sign out" onClick={props.logout}/>
        </IconMenu>
    </div>
);