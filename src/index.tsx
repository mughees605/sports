import * as React from "react";
import * as ReactDOM from "react-dom";
import { Router, Route, IndexRoute, hashHistory, browserHistory } from 'react-router'
import { Provider } from 'react-redux';

import appConfig from './config/index';
import * as firebase from "firebase";
firebase.initializeApp(appConfig.firebaseDev);

// material-ui -- start --
import * as injectTapEventPlugin from 'react-tap-event-plugin/src/injectTapEventPlugin'; 
injectTapEventPlugin();
// material-ui -- end --


import { store } from './store/index';
import { App, Home, EventContainer, SignupContainer } from "./container";


// function checkIsAdmin(nextState: any, replace: Function) {
//     let user = JSON.parse(localStorage.getItem("ngo"));
//     if (user && user.type !== 'admin') {
//         replace({
//             pathname: "/home",
//             state: { nextPathname: nextState.location.pathname }
//         })
//     } else if (user == null) {
//         replace({
//             pathname: "/home",
//             state: { nextPathname: nextState.location.pathname }
//         })
//     }
// }

ReactDOM.render(
    <Provider store={store}>

        <Router history={browserHistory}>
            <Route path="/" component={App}>
                <IndexRoute component={Home} />
                <Route path="home" component={Home} />
                <Route path="event" component={EventContainer} />
                <Route path="signup" component={SignupContainer} />
                {/*<Route path="login" component={Login} />
                <Route path="/a" component={AdminDashboard} >
                    <IndexRoute component={AdminPanel} onEnter={checkIsAdmin}/>
                    <Route path="ngo" component={NgoCreate} onEnter={checkIsAdmin}/>
                    <Route path="list-ngo" component={NgoList} onEnter={checkIsAdmin}/>
                    <Route path="ngo-detail/:ngoId" component={NgoDetail} />
                </Route>
                <Route path="counter" component={Counter} />
                <Route path="signup" component={Signup} />*/}
            </Route>
        </Router>

    </Provider>,
    document.getElementById('root')
);